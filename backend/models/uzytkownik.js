const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const bcrypt = require('bcrypt-nodejs');

var UzytkownikSchema = new Schema({
    imie: {
        type: String,
        required: true
    },
    nazwisko: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    haslo: {
        type: String,
        required: true
    }
}, {collection: 'uzytkownicy'});

UzytkownikSchema.pre('save', function (next) {
    let user = this;
    if (this.isModified('haslo') || this.isNew) {
        bcrypt.genSalt(10, function (err, salt) {
            if (err) {
                return next(err);
            }
            bcrypt.hash(user.haslo, salt, null, function (err, hash) {
                if (err) {
                    return next(err);
                }
                user.haslo = hash;
                next();
            });
        });
    } else {
        return next();
    }
});

UzytkownikSchema.methods.porownajHasla = function (haslo, cb) {
    bcrypt.compare(haslo, this.haslo, (err, isMatch) => {
        if (err) {
            return cb(err);
        }
        cb(null, isMatch);
    });
};

module.exports = mongoose.model('Uzytkownik', UzytkownikSchema);