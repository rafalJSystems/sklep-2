const express = require('express');
const router = express.Router();
const Uzytkownik = require('./../models/uzytkownik');
const jwt = require('jsonwebtoken');

router.post('/rejestracja', (req, res) => {
    if (
        !req.body.imie ||
        !req.body.nazwisko ||
        !req.body.email ||
        !req.body.haslo
    ) {
        res.json( { success: false, msg: 'Podaj wszystkie pola'} );
    } else {
        let user = new Uzytkownik({
            imie: req.body.imie,
            nazwisko: req.body.nazwisko,
            email: req.body.email,
            haslo: req.body.haslo
        });

        user.save( (err) => {
            if (err) throw err;

            res.json( { success: true, msg: 'Dodany uzytkownik' } );
        });
    }
});

router.post('/logowanie', (req, res) => {
    Uzytkownik.findOne({email: req.body.email}, (err, user) => {
        if (err) throw err;

        if (!user) {
            res.json({ success: false, msg: 'Niepoprawny email lub hasło' });
        } else {
            user.porownajHasla(req.body.haslo, (err, isMatch) => {
                if (isMatch && !err) {
                    let token = jwt.sign(user, 'nodeauthsecret');
                    res.json( { success: true, token: 'JWT ' + token } );
                } else {
                    res.status(401).send({ success: false, msg: 'Niepoprawny email lub hasło' });
                }
            });
        }
    });
});

module.exports = router;
