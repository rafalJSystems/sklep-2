import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';

import { ProduktyService } from './../produkty.service';

import 'rxjs/add/operator/switchMap';

@Component({
  selector: 'app-produkt',
  templateUrl: './produkt.component.html',
  styleUrls: ['./produkt.component.css'],
  providers: [ ProduktyService ]
})
export class ProduktComponent implements OnInit {
  produkt: any;

  constructor(
    private produktyService: ProduktyService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.route.paramMap
      .switchMap((params: ParamMap) => {
        return this.produktyService.getProdukt(params.get('id'));
      })
      .subscribe(produkt => this.produkt = produkt);
  }

}
