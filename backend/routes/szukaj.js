const express = require('express');
const router = express.Router();
const Produkt = require('./../models/produkt');

router.get('/', (req, res) => {
    if (req.query.szukaj) {
        Produkt.find({
            nazwa: {
                '$regex': new RegExp(req.query.szukaj, 'i')
            }
        }, (err, items) => {
            if (err) throw err;

            res.json(items);
        });
    } else {
        res.json(null);
    }
});

module.exports = router;