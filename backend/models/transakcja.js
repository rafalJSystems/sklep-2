const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var TransakcjaSchema = new Schema({
    data: {
        type: Date,
        default: Date.now,
        required: true
    },
    suma: {
        type: Number,
        required: true
    },
    produkty: [
        {
            produkt_id: {
                type: Schema.Types.ObjectId,
                ref: 'Produkt',
                required: true
            },
            ilosc: {
                type: Number,
                required: true
            }
        }
    ],
    uzytkownik: {
        type: Schema.Types.ObjectId,
        ref: 'Uzytkownik',
        required: true
    }
}, { collection: 'transakcje'});

module.exports = mongoose.model('Transakcja', TransakcjaSchema);