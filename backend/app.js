const express = require('express');
const path = require('path');
const favicon = require('serve-favicon');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const passport = require('passport');
require('./config/passport')(passport);

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/sklep', {
  useMongoClient: true
});
mongoose.set('debug', true);

var app = express();

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(passport.initialize());

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept');
  next();
});

var index = require('./routes/index');
var kategoria = require('./routes/kategoria');
var szukaj = require('./routes/szukaj');
var produkt = require('./routes/produkt');
var autoryzacja = require('./routes/autoryzacja');
var zakup = require('./routes/zakup.js');

app.use('/', index);
app.use('/api/kategoria/', kategoria);
app.use('/api/szukaj/', szukaj);
app.use('/api/produkt/', produkt);
app.use('/api/auth/', autoryzacja);
app.use('/api/zakup/', zakup);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // render the error page
  res.status(err.status || 500);
  res.json({
    message: err.message,
    error: req.app.get('env') === 'development' ? err : {}
  });
});

module.exports = app;
