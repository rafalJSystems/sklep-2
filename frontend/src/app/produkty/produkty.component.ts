import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';

import { KategorieService } from './../kategorie.service';
import { ProduktyService } from './../produkty.service';

import 'rxjs/add/operator/switchMap';

@Component({
  selector: 'app-produkty',
  templateUrl: './produkty.component.html',
  styleUrls: ['./produkty.component.css'],
  providers: [KategorieService, ProduktyService]
})
export class ProduktyComponent implements OnInit {
  podkategorie: [any];
  marki: [any];

  constructor(
    private kategorieService: KategorieService,
    private produktyService: ProduktyService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.route.paramMap
      .switchMap((params: ParamMap) => {
        return this.kategorieService.getKategoria(params.get('slug'));
      })
      .subscribe(kategoria => {
        this.podkategorie = kategoria.podkategorie
        this.produktyService.getProduktyBy({kat: kategoria._id}).then((result) => {
          console.log(result);
        });
      });
  }

}
