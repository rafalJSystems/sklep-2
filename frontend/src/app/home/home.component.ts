import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { ProduktyService } from './../produkty.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [ ProduktyService ]
})
export class HomeComponent implements OnInit {
  produkty: any;

  constructor(
    private produktyService: ProduktyService,
    private router: Router
  ) { }

  ngOnInit() {
    this.produktyService.getProdukty().then(result => {
      this.produkty = result.wynik;
    });
  }

  gotoProdukt(id): void {
    this.router.navigate(['/produkt', id]);
  }
}
