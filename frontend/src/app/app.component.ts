import { Component, OnInit } from '@angular/core';

import { KategorieService } from './kategorie.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [ KategorieService ]
})
export class AppComponent implements OnInit {
  kategorie: any;

  constructor(
    private kategorieService: KategorieService
  ) { }
  
  ngOnInit(): void {
    this.kategorieService.getKategorie().then(result => {
      this.kategorie = result;
    });
  }
}
