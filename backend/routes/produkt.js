const express = require('express');
const router = express.Router();
const Produkt = require('./../models/produkt');

router.get('/', (req, res) => {
    let query = {};
    let options = { limit: 6 };

    if (req.query.kat) {
        query.kategoria = req.query.kat;
        options = {};
    }
    if (req.query.pod) {
        query.podkategoria = req.query.pod;
        options = {};
    }
    if (req.query.cena) {
        query.cena = {
            '$gte': req.query.cena[0],
            '$lte': req.query.cena[1]
        };
        options = {};
    }
    if (req.query.marki) {
        query.marka = {
            '$in': req.query.marki
        };
        options = {};
    }
    if (req.query.wiek) {
        query.wiek = {
            '$gte': req.query.wiek[0],
            '$lte': req.query.wiek[1]
        };
        options = {};
    }

    Produkt.find(query, {}, options, (err, result) => {
        if (err) throw err;

        let marki = [];
        for (let m of result) {
            if (marki.indexOf(m.marka) < 0) {
                marki.push(m.marka);
            }
        }

        res.json({
            marki: marki,
            wynik: result
        });
    });
});

router.get('/:id', (req, res) => {
    Produkt.findOne({_id: req.params.id}, (err, item) => {
        if (err) throw err;

        res.json(item);
    });
});

module.exports = router;
