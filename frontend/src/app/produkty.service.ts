import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

@Injectable()
export class ProduktyService {
  private headers = new Headers({'Content-Type': 'application/json'});
  private apiURL = 'http://localhost:3000/api/produkt/';

  constructor(private http: Http) { }

  getProdukty(): Promise<any> {
    return this.http.get(this.apiURL)
      .toPromise()
      .then(response => response.json())
      .catch(this.handleError);
  }

  getProdukt(id: any): Promise<any> {
    return this.http.get(this.apiURL + id)
      .toPromise()
      .then(response => response.json())
      .catch(this.handleError);
  }

  getProduktyBy(filtr: any): Promise<any> {
    return this.http.get(this.apiURL, JSON.stringify(filtr))
      .toPromise()
      .then(response => response.json())
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.log(error);
    return Promise.reject(error);
  }
}
