import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './../home/home.component';
import { ProduktyComponent } from './../produkty/produkty.component';
import { ProduktComponent } from './../produkt/produkt.component';
import { KoszykComponent } from './../koszyk/koszyk.component';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full'},
  { path: 'home', component: HomeComponent },
  { path: 'produkty/:slug', component: ProduktyComponent },
  { path: 'produkt/:id', component: ProduktComponent },
  { path: 'koszyk', component: KoszykComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})

export class RoutingModule { }
