const express = require('express');
const router = express.Router();
const Transakcja = require('./../models/transakcja');
const passport = require('passport');
const jwt = require('jsonwebtoken');

router.post('/',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    if (
      !req.body.suma ||
      !req.body.produkty ||
      !req.body.uzytkownik
    ) {
      res.json( { success: false, msg: "Podaj wszystkie pola" });
    } else {
      let transakcja = new Transakcja({
        suma: req.body.suma,
        produkty: req.body.produkty,
        uzytkownik: req.body.uzytkownik
      });

      transakcja.save(() => {
        res.json( { success: true, msg: "Dodano transakcje" });
      });
    }
});

module.exports = router;