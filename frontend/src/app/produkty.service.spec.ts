import { TestBed, inject } from '@angular/core/testing';

import { ProduktyService } from './produkty.service';

describe('ProduktyService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ProduktyService]
    });
  });

  it('should be created', inject([ProduktyService], (service: ProduktyService) => {
    expect(service).toBeTruthy();
  }));
});
